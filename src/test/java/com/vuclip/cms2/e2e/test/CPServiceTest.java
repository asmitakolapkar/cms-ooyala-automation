package com.vuclip.cms2.e2e.test;

import static org.testng.Assert.assertTrue;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.google.gson.JsonParser;
import com.vuclip.cms2.helper.CPHelper;
import com.vuclip.cms2.validator.CPValidator;

import io.restassured.response.Response;

@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
public class CPServiceTest extends AbstractTestNGSpringContextTests {

	private final Logger logger = LoggerFactory.getLogger(this.getClass());
	
	JsonParser parser = new JsonParser();

	@Autowired
	CPHelper cpHelper;

	@Autowired
	CPValidator cpValidator;

	Long rmsId = 1000L;
	
	@BeforeClass
	public void init() {
		
	}
	
	@Test(groups = "Cp_Ms", enabled = false, dependsOnGroups = "cpCreate")
	public void verifyCpCreateEvent() {
		try {

			// Call to test data service for expected
			Response response = cpHelper.getExpectedTestData("cp_create", 1);

			// Validate product push data against service response
			cpValidator.validateCpService(rmsId, response);

		} catch (Exception e) {
			assertTrue(false, e.getMessage());

			logger.error("Exception>>>>>>>>>", e);
		}
	}
	
	@Test(groups = "Cp_Ms", enabled = true)
	public void verifyCpUpdateEvent() {
		try {

			Response response = cpHelper.getExpectedTestData("cp_update", 1);

			cpValidator.validateCpService(rmsId, response);

		} catch (Exception e) {
			assertTrue(false, e.getMessage());

			logger.error("Exception>>>>>>>>>", e);
		}
	}

	@Test(groups = "Cp_Ms", enabled = false)
	public void verifyCpDeleteEvent() {
		try {

			Response response = cpHelper.getExpectedTestData("cp_delete", 1);

			cpValidator.validateCpService(rmsId, response);

		} catch (Exception e) {
			assertTrue(false, e.getMessage());

			logger.error("Exception>>>>>>>>>", e);
		}
	}

}
