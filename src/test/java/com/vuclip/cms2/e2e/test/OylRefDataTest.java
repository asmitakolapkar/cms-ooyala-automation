package com.vuclip.cms2.e2e.test;

import static org.testng.Assert.assertTrue;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.testng.annotations.Test;

import com.vuclip.cms2.entity.CountryEntity;
import com.vuclip.cms2.entity.LanguageEntity;
import com.vuclip.cms2.helper.CountryHelper;
import com.vuclip.cms2.helper.LanguageHelper;
import com.vuclip.cms2.validator.RefDataValidator;

import io.restassured.response.Response;


@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
public class OylRefDataTest extends AbstractTestNGSpringContextTests{
	
	private final Logger logger = LoggerFactory.getLogger(this.getClass());
	
	@Autowired
	CountryHelper countryHelper;
	
	@Autowired
	LanguageHelper languageHelper;
	
	@Autowired
	RefDataValidator refDataValidator;
	
	Long cmsId = 103L;
	
	String code = "LnagCode";
	
	@Test(groups = "RefData", enabled = true)
	public void verifyCountryCreateEvent() {
		try {
			//validate  db entry and expected
			CountryEntity country = countryHelper.getCountry(cmsId);

			Response response = countryHelper.getExpectedTestData("country_create", 1);

			refDataValidator.validateCountryService(country, response);
			
			//get country by id
			Response rsp = countryHelper.getCountryById(cmsId);
			
			Response expectedResponse = countryHelper.getExpectedTestData("country_get", 1);
			
			refDataValidator.validateResponse(expectedResponse, rsp);

		} catch (Exception e) {
			assertTrue(false, e.getMessage());

			logger.error("Exception>>>>>>>>>", e);
		}
	}
	
	@Test(groups = "RefData", enabled = false)
	public void verifyCountryUpdateEvent() {
		try {
			
			CountryEntity country = countryHelper.getCountry(cmsId);

			Response response = countryHelper.getExpectedTestData("country_update", 1);

			refDataValidator.validateCountryService(country, response);

		} catch (Exception e) {
			assertTrue(false, e.getMessage());

			logger.error("Exception>>>>>>>>>", e);
		}
	}
	
	@Test(groups = "RefData", enabled = false)
	public void verifyCountryDeleteEvent() {
		try {
			
			CountryEntity country = countryHelper.getCountry(cmsId);

			Response response = countryHelper.getExpectedTestData("country_delete", 1);

			refDataValidator.validateCountryService(country, response);

		} catch (Exception e) {
			assertTrue(false, e.getMessage());

			logger.error("Exception>>>>>>>>>", e);
		}
	}

	@Test(groups = "RefData", enabled = false)
	public void verifyLangCreateEvent() {
		try {

			LanguageEntity lang = languageHelper.getLang(cmsId);
			
			Response response = languageHelper.getExpectedTestData("language_create", 1);

			refDataValidator.validateLanguageService(lang, response);
			
			// get lang by id
			Response rspbyId = languageHelper.getLanguageById(cmsId);
			
			Response expectedResponse = languageHelper.getExpectedTestData("language_get", 1);
			
			refDataValidator.validateResponse(expectedResponse, rspbyId);
			
			// get lang by code 
			Response rspbyCode = languageHelper.getLanguageByCode(code);
			
			refDataValidator.validateResponse(expectedResponse, rspbyCode);

		} catch (Exception e) {
			assertTrue(false, e.getMessage());

			logger.error("Exception>>>>>>>>>", e);
		}
	}
	
	@Test(groups = "RefData", enabled = false)
	public void verifyLangUpdateEvent() {
		try {

			LanguageEntity lang = languageHelper.getLang(cmsId);
			
			Response response = languageHelper.getExpectedTestData("language_update", 1);

			refDataValidator.validateLanguageService(lang, response);

		} catch (Exception e) {
			assertTrue(false, e.getMessage());

			logger.error("Exception>>>>>>>>>", e);
		}
	}
	
	@Test(groups = "RefData", enabled = false)
	public void verifyLangDeleteEvent() {
		try {

			LanguageEntity lang = languageHelper.getLang(cmsId);
			
			Response response = languageHelper.getExpectedTestData("language_delete", 1);

			refDataValidator.validateLanguageService(lang, response);

		} catch (Exception e) {
			assertTrue(false, e.getMessage());

			logger.error("Exception>>>>>>>>>", e);
		}
	}
	
}
