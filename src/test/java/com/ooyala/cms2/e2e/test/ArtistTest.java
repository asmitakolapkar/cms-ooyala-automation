package com.ooyala.cms2.e2e.test;

//import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.ooyala.ui.pages.ArtistPage;
import com.ooyala.ui.pages.OylHomePage;
import com.ooyala.ui.pages.OylLoginPage;


public class ArtistTest extends ArtistPage{
	OylLoginPage oylLoginPage;
	OylHomePage oylhomePage;
	ArtistPage artistpage;
	
	@BeforeMethod
	public void setup() throws InterruptedException {
		initialization("flexUrl");
		oylLoginPage = new OylLoginPage();
		oylhomePage = oylLoginPage.login(prop.getProperty("flexUsername"), prop.getProperty("flexPassword"));
	
	}
	
	@Test(priority=1)
	public void verifyAddNewArtist() throws Exception{
		oylhomePage.clickNewButton();
		artistpage= oylhomePage.selectArtistOption();
		artistpage.setName("Shroff");
		artistpage.setDescription("Here goes the description for Shroff");
		artistpage.setCmsId("118");
		artistpage.saveButtonClick();
		
	}
	
}
