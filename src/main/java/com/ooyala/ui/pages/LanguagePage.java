package com.ooyala.ui.pages;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.openqa.selenium.support.PageFactory;
import com.ooyala.ui.pageObjects.LanguageObject;

public class LanguagePage extends LanguageObject{
	private final Logger log = LoggerFactory.getLogger(this.getClass());
	// Initializing the Page Objects:
	public LanguagePage() {
		PageFactory.initElements(driver, this);
	}
		
	//enter name
	public void enterLanguageName(String languageName) {
		langName.clear();
		langName.sendKeys(languageName);
	}
		
	//enter description
	public void enterDescription(String languageDesc) {
		langDesc.sendKeys(languageDesc);
	}
	
	//enter cmsid
	public void enterCmsId(long cmsIdvalue) {
		cmsId.sendKeys( String.valueOf(cmsIdvalue));
	}
	
	//enter display name
	public void enterDisplayName(String displName) {
		displayName.sendKeys(displName);
	}
	
	//enter display name
	public void enterLangCode(String languageCode) {
		langCode.sendKeys(languageCode);
	}
	
	//save record
	public void saveLanguageRecord() {
		saveBtnCreate.click();
	}
	
	//method to check text present in summary table
	public boolean checkLanguageCreated(String langName) {
		try {
			checkSummaryTable(langName).isDisplayed();
			return true;
		}catch(org.openqa.selenium.NoSuchElementException e) {
			return false;
		}
	}
	
	//method to check text present in summary table
	public void OpenLangauge(String langName) {
		try {
			if(checkSummaryTable(langName).isDisplayed()) {
				checkSummaryTable(langName).click();
				waitForElement(deleteBtn);
			}
		}catch(org.openqa.selenium.NoSuchElementException e) {
			log.error("*******The language text not found to update*******");
		}
	}
	
	//click edit button
	public void clickEditBtn() {
		editLangButton.click();
	}
	
	//Click save button to  update
	public void clickUpdationSavebtn() {
		saveBtnUpdate.click();
	}
	
	//Click delete button
	public void clickAndConfirmDelete() throws InterruptedException {
		deleteBtn.click();
		waitForElement(confirmDelete);
		confirmDelete.click();
	}
}
