package com.ooyala.ui.pages;

import org.openqa.selenium.support.PageFactory;
import org.springframework.stereotype.Component;

import com.ooyala.ui.pageObjects.OylLoginPageObject;

@Component
public class OylLoginPage extends OylLoginPageObject{
	
	
	//Initializing the Page Objects:
	public OylLoginPage(){
		PageFactory.initElements(driver, this);
	}
	
	public OylHomePage login(String usrname, String pwd) {
		username.sendKeys(usrname);
		password.sendKeys(pwd);
		loginbtn.click();
		return new OylHomePage();
	}
}
