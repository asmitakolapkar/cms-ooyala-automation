package com.ooyala.ui.pages;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.openqa.selenium.support.PageFactory;

import com.ooyala.ui.pageObjects.CpObject;

@Component
public class CpPage extends CpObject{
	private final Logger log = LoggerFactory.getLogger(this.getClass());
	
	// Initializing the Page Objects:
	public CpPage() {
		PageFactory.initElements(driver, this);
	}
	
	
	//enter name
	public void enterName(String name) {
		cpName.clear();
		cpName.sendKeys(name);
	}
	
	//enter description
	public void enterDescription(String desc) {
		description.sendKeys(desc);
	}
	
	//enter rmsid
	public void enterRmsId(long rmsIdvalue) {
		rmsCpId.sendKeys( String.valueOf(rmsIdvalue));
	}
	
	//enter cmsid
	public void enterCmsId(long cmsIdvalue) {
		cmsCpId.sendKeys( String.valueOf(cmsIdvalue));
	}
	
	//select baton profile id  1
	public void selectProfile(String pname) {
		selectbatonprofileRadioBtn(pname).click();
		
	}
	
	//enter baton name
	public void enterBatonName(String batonName) {
		batonPrName.sendKeys(batonName);
	}
	
	
	//enter cp user name
	public void entercpUserName(String cpUserName) {
		contentUserName.sendKeys(cpUserName);
	}
	
	//enter cp manager name
	public void enterMngrName(String mngrName) {
		cpMngrName.sendKeys(mngrName);
	}
	
	//enter sec cp manager name
	public void enterSecMngrName(String secMngrName) {
		secCpMngrName.sendKeys(secMngrName);
	}
	
	//enter cp manager name
	public void enterCpCountry(String countryValue) {
		cpCountry.sendKeys(countryValue);
	}
	
	//enter cp Spoc Name
	public void enterSpocName(String spocName) {
		cpSpocName.sendKeys(spocName);
	}
	
	//enter cp Spoc email
	public void enterSpocEmail(String email) {
		cpEmail.sendKeys(email);
	}
	
	//enter cp ContactNo
	public void enterContact(String cpContact) {
		cpContactNo.sendKeys(cpContact);
	}
	
	//enter cp Ops Spoc Name
	public void enterOpsSpocName(String opsSpocName) {
		cpOpsSpocName.sendKeys(opsSpocName);
	}
		
	//enter cp Ops Spoc email
	public void enterOpsSpocEmail(String opsSpocEmail) {
		cpOpsSpocEmail.sendKeys(opsSpocEmail);
	}
		
	//enter cp Ops  ContactNo
	public void enterOpsContact(String opsContact) {
		cpOpsContact.sendKeys(opsContact);
	}
	
	//check cp active
	public void activateCp() {
		if(!cpIsActive.isSelected())
		cpIsActive.click();
		
	}

	
	//save record
	public void saveCpRecord() {
		saveBtnCreate.click();
	}
	
	//method to check text present in summary table
	public void OpenCp(String cp) {
		try {
			if(checkSummaryTable(cp).isDisplayed()) {
				checkSummaryTable(cp).click();
				waitForElement(deleteBtn);
			}
		}catch(org.openqa.selenium.NoSuchElementException e) {
			log.error("*******The cp text not found to update*******");
		}
	}
		
	//click edit button
	public void clickEditBtn() {
		editButton.click();
	}
		
	//Click save button to  update
	public void clickUpdationSavebtn() {
		saveBtnUpdate.click();
	}
	
	//method to check text present in summary table
	public boolean checkCpCreated(String cpName) {
		try {
			checkSummaryTable(cpName).isDisplayed();
			return true;
		}catch(org.openqa.selenium.NoSuchElementException e) {
			return false;
		}
	}
	
	//Click delete button
	public void clickAndConfirmDelete() throws InterruptedException {
		deleteBtn.click();
		waitForElement(confirmDelete);
		confirmDelete.click();
	}
}
