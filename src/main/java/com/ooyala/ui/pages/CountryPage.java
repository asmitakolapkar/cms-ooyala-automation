package com.ooyala.ui.pages;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;
import com.ooyala.ui.pageObjects.CountryObject;

public class CountryPage extends CountryObject {
	
	private final Logger log = LoggerFactory.getLogger(this.getClass());
	
	// Initializing the Page Objects:
	public CountryPage() {
		PageFactory.initElements(driver, this);
	}
	
	//create/update name
	public void enterName(String countryName) {
		name.clear();
		name.sendKeys(countryName);
	}
	
	//enter description
	public void enterDescription(String countryDesc) {
		description.sendKeys(countryDesc);
	}
	
	//enter cmsid
	public void enterCmsId(long cmsIdvalue) {
		cmsId.sendKeys( String.valueOf(cmsIdvalue));
	}
	
	//enter a2Code
	public void enterA2Code(String a2codeValue) {
		a2Code.sendKeys(a2codeValue);
	}
	
	//enter a3Code
	public void enterA3Code(String a3codeValue) {
		a3Code.sendKeys(a3codeValue);
	}
	
	//enter currency
	public void enterCurrency(String currencyValue) {
		currency.sendKeys(currencyValue);
	}
	
	//Enter LongName
	public void enterLongName(String longnameVal) {
		longName.sendKeys(longnameVal);
	}
	
	//Enter Region
	public void selectRegion(String regionVal) {
		Select dropdown = new Select(region);
		dropdown.selectByVisibleText(regionVal);
	}
	
	public void clickCreationSavebtn() {
		saveButtonCreate.click();
	}
	
	//method to check text present in summary table
	public boolean checkCountryPresent(String countryName) {
		try {
			if(checkSummaryTable(countryName).isDisplayed());
		 	return true;
		}catch(org.openqa.selenium.NoSuchElementException e) {
			return false;
		}
	}
	
	//method to check text present in summary table
	public void OpenCountry(String countryName) {
		try{
			if(checkSummaryTable(countryName).isDisplayed()) {
			checkSummaryTable(countryName).click();
			waitForElement(deleteBtn);
			}
		}catch(org.openqa.selenium.NoSuchElementException e){
			log.error("*******The country text not found to update*******");
		}
	}
	
	//click edit button
	public void clickEditBtn() {
		editButton.click();
	}
	
	//Click save button to  update
	public void clickUpdationSavebtn() {
		saveBtnUpdate.click();
		//driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
	}
	
	//Click delete button and confirm deletion
	public void clickAndConfirmDelete() throws InterruptedException {
		deleteBtn.click();
		waitForElement(confirmDelete);
		confirmDelete.click();
	}
}