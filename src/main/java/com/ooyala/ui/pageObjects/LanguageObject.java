package com.ooyala.ui.pageObjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import com.cms.ui.base.TestBase;

public class LanguageObject extends TestBase{
	
	//Langauge form fields
	
	@FindBy(name="name")
	public
	WebElement langName;
	
	@FindBy(name="description")
	public
	WebElement langDesc;
	
	@FindBy(name="____mio_field____root:cms-language-id[0]")
	public
	WebElement cmsId;
	
	@FindBy(name="____mio_field____root:language-display-name[1]")
	public
	WebElement displayName;
	
	@FindBy(name="____mio_field____root:language-code[2]")
	public
	WebElement langCode;
	
	
	@FindBy(id="submit_control-2-button")
	public
	WebElement saveBtnCreate;
	
	
	@FindBy(id = "edit_summary")
	protected
	WebElement editLangButton;
	
	@FindBy(id = "submit_control_bottom")
	protected
	WebElement saveBtnUpdate;
	
	@FindBy(id = "delete")
	public
	WebElement deleteBtn;
	
	@FindBy(id = "yes_control")
	public
	WebElement confirmDelete;
	
	
	public static WebElement checkSummaryTable(String langName){
		   return driver.findElement(By.xpath("//*[@id='userobject_search_language_datatable']//table//tbody[2]/tr/td[4]/div[contains(text(),'"+langName+"')]"));
	}
}
