package com.ooyala.ui.pageObjects;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import com.cms.ui.base.TestBase;

public class OylHomePageObject extends TestBase{
	
	//country tab
    @FindBy(xpath = "//*[@id='nav_top']/ul/li[5]/a")
    public
	WebElement countryTab;
		
	//newButton
	@FindBy(xpath = "//*[@id='new_button-button']")
	public
	WebElement newButton;
	
	//******Country********//
	
	//select country option in new dropdown
	@FindBy(xpath = "//*[@id='new_panel']//tr[6]//a")
	public
	WebElement countryOption;
	
	//Search box on home page
	@FindBy(id="search_text")
	public
	WebElement searchbox;
	
	//Search box on home page
	@FindBy(id="search_text_control-button")
	public
	WebElement searchBtn;
	
	
	//******Language********//
	
	//select Language option in new dropdown
	@FindBy(xpath = "//*[@id='new_panel']//tr[12]//a")
	public
	WebElement languageOption;
	
	//select Language option in new dropdown
	@FindBy(xpath = "//*[@id='nav_top']/ul/li[7]/a")
    public
	WebElement languageTab;
	
	
	//******CP********//
	@FindBy(xpath = "//*[@id='new_panel']//tr[5]//a")
	public
	WebElement cpOption;
	
	//select cp option in new dropdown
	@FindBy(xpath = "//*[@id='nav_top']/ul/li[4]/a")
    public
	WebElement cpTab;
}
