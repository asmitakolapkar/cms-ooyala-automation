package com.ooyala.ui.pageObjects;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import com.cms.ui.base.TestBase;

public class OylLoginPageObject extends TestBase{
	

	@FindBy(id="username")
	public
	WebElement username;

	@FindBy(id="password")
	public
	WebElement password;
	
	@FindBy(xpath="//*[@class='flex-button primary large']")
	public
	WebElement loginbtn;
	
}
