package com.vuclip.cms2.dao;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.vuclip.cms2.entity.LanguageEntity;

@Repository
public interface LanguageRepository extends JpaRepository<LanguageEntity, Long> {

	@Modifying
	@Query(value = "truncate table ct_language", nativeQuery = true)
	public void truncateLanguage();

	Optional<LanguageEntity> findById(Long id);

}