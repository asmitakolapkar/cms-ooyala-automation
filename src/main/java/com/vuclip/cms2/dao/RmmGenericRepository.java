package com.vuclip.cms2.dao;

import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.vuclip.cms2.entity.RmmGenericEntity;

@Repository
public interface RmmGenericRepository extends JpaRepository<RmmGenericEntity, Long> {

	@Transactional
	@Modifying
	@Query(value = "delete from rmm_generic where cpid =:rmsid", nativeQuery = true)
	public void deleteCp(@Param("rmsid") Long rmsid);
	
	@Modifying
	@Query(value = "truncate table rmm_generic", nativeQuery = true)
	public void truncateRmmGeneric();

	Optional<RmmGenericEntity> findByRmsid(Long rmsid);
}
