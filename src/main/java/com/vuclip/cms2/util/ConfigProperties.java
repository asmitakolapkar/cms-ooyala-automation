package com.vuclip.cms2.util;

import javax.validation.constraints.NotNull;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;
import org.springframework.validation.annotation.Validated;

import lombok.Getter;
import lombok.Setter;

@Component
@ConfigurationProperties(prefix = "cms.service")
@Validated
@Getter
@Setter
public class ConfigProperties {

	@NotNull(message = "Service Host configuration is mandatory")
	private Host host;

	@NotNull(message = "Pubsub configuration is mandatory")
	private Pubsub pubsub;

	@Getter
	@Setter
	public static class Host {

		private String clipHost;
		private String catalogHost;
		private String artistHost;
		private String productHost;
		private String refDataHost;
		private String cpHost;
		private String mockHost;
		private String testDataHost;

	}

	@Getter
	@Setter
	public static class Pubsub {

		private String clipTopic;
		private String catalogTopic;
		private String seasonTopic;
		private String showTopic;
		private String showHierarchyTopic;
		private String artistTopic;
		private String productTopic;
		private String languageTopic;
		private String countryTopic;
		private String cpTopic;
		private String caasClipTopic;
		private String caasPlaylistTopic;
		private String caasArtistTopic;
		private String cpfTopic;

	}

}
