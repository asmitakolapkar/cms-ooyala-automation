/**
 * 
 */
package com.vuclip.cms2.util;

/**
 * @author Akshay Gupta
 *
 *         26-Jun-2018
 */
public class XmlNodeNotFoundException extends Exception {

	public XmlNodeNotFoundException(String s) {
		super(s);
	}

}
