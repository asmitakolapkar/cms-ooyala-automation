package com.vuclip.cms2.helper;

import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.google.gson.JsonObject;
import com.vuclip.cms2.dao.LanguageRepository;
import com.vuclip.cms2.entity.LanguageEntity;
import com.vuclip.cms2.services.RestClientService;
import com.vuclip.cms2.util.ConfigProperties;

import io.restassured.response.Response;

@Component
public class LanguageHelper extends BaseHelper{

	@Autowired
	RestClientService restClientService;

	@Autowired
	ConfigProperties configProperties;

	@Autowired
	LanguageRepository languageRepo;
	
	@Transactional
	public void truncateLanguage() {
		
		languageRepo.truncateLanguage();;
	}

	public LanguageEntity getLang(Long id) throws InterruptedException {

		Optional<LanguageEntity> languageEntity = languageRepo.findById(id);

		for (int i = 0; i < 10; i++) {

			if (!languageEntity.isPresent() || 
					(System.currentTimeMillis() - languageEntity.get().getUpdatetime().getTime() < 5000L)) {
				Thread.sleep(500);

				languageEntity = languageRepo.findById(id);

			} else {
				break;
			}
		}

		return languageEntity.get();
	}
	
	public Response getLanguages() throws Exception {

		Response response = restClientService.getLanguages();

		return response;

	} 
	
	public Response getLanguageById(Long id) throws Exception {

		Response response = restClientService.getLanguageById(id);

		return response;

	} 
	
	public Response getLanguageByCode(String code) throws Exception {

		Response response = restClientService.getLanguageByCode(code);

		return response;

	} 
	
	@Override
	public void pushInputPubsubMessage(JsonObject message) {

		restClientService.pushDataToPubsubTopic(configProperties.getPubsub().getLanguageTopic(), message);

	}
	
}
