package com.vuclip.cms2.helper;

import java.io.StringReader;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.transaction.Transactional;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.w3c.dom.Document;
import org.xml.sax.InputSource;

import com.google.gson.JsonObject;
import com.vuclip.cms2.model.RequestData;
import com.vuclip.cms2.services.RestClientService;
import com.vuclip.cms2.util.ConfigProperties;

import io.restassured.response.Response;

@Component
public abstract class BaseHelper {

	@Autowired
	RestClientService restClientService;

	@Autowired
	ConfigProperties configProperties;

	@PersistenceContext
	private EntityManager em;

	@Transactional
	public void executePrerequisitiesSqlData(String tcName) throws Exception {

		Response response = restClientService.getPrerequisitiesSql(tcName);

		Object[] sqlArray = response.getBody().as(Object[].class);

		executeSql(sqlArray);

	}

	public List<RequestData> getInputTestData(String tcName) throws Exception {

		Response response = restClientService.getInputTestData(tcName);

		List<RequestData> inputList = Arrays.asList(response.getBody().as(RequestData[].class));

		return inputList;

	}

	public Response getExpectedTestData(String tcName, int serialNumber) throws Exception {

		Response response = restClientService.getExpectedTestData(tcName, serialNumber);

		return response;

	}

	public Response getPubsubConsumerData(HashMap queryMap) throws Exception {

		Response response = restClientService.getConsumerPubsubMessage(queryMap);

		return response;

	}

	public Response removePubsubConsumerData(HashMap queryMap) throws Exception {

		Response response = restClientService.removePubsubMessage(queryMap);

		return response;

	}

	public void executeSql(Object[] queryList) {

		for (int i = 0; i < queryList.length; i++) {

			String queryString = queryList[i].toString();
			Query q = em.createNativeQuery(queryString);

			q.executeUpdate();
		}

	}

	public Document convertStringToXMLDocument(String xmlString) {
		// Parser that produces DOM object trees from XML content
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();

		// API to obtain DOM Document instance
		DocumentBuilder builder = null;
		try {
			// Create DocumentBuilder with default configuration
			builder = factory.newDocumentBuilder();

			// Parse the content to Document object
			Document doc = builder.parse(new InputSource(new StringReader(xmlString)));
			return doc;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	public abstract void pushInputPubsubMessage(JsonObject message);

}
