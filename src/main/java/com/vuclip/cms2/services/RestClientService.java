package com.vuclip.cms2.services;

import java.util.HashMap;

import org.springframework.stereotype.Service;

import io.restassured.response.Response;

@Service
public interface RestClientService {

	Response getPrerequisitiesSql(String tcName);

	Response getInputTestData(String tcName);

	Response getExpectedTestData(String tcName, int serialNumber);

	Response pushDataToPubsubTopic(String topicName, Object message);

	Response getCPService(Long id);

	Response getConsumerPubsubMessage(HashMap<String, Object> parametersMap);

	Response getShowService(Long showId);

	Response removePubsubMessage(HashMap<String, Object> parametersMap);
	
	// Ref data
	
	Response getLanguages();
	
	Response getLanguageByCode(String languageCode);
	
	Response getLanguageById(Long id);
	
	Response getCounries();
	
	Response getCountryById(Long id);
	
	Response getRatings();
	
	Response getAgeRating();
	
	Response getRegionalAgeRating();
	
	Response getRegion();
	
	Response getRegionsForAgerating();
	
	// clip
	
	Response getSubtitle(Long id);
	
	Response getSubtitleByType(Long id, String type);
	
	Response pushClipToCaas(Long id);
	
	Response pushClipToContext(Long id);
	
	Response getRightsById(Long id);

}
