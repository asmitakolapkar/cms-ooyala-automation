package com.vuclip.cms2.validator;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.testng.asserts.SoftAssert;

import com.vuclip.cms2.entity.ContentPartnerEntity;
import com.vuclip.cms2.entity.RmmGenericEntity;
import com.vuclip.cms2.helper.CPHelper;
import com.vuclip.cms2.model.DbMap;
import io.restassured.response.Response;

@Component
public class CPValidator extends BaseValidator  {
	
	@Autowired
	CPHelper cpHelper;
	
	private final Logger logger = LoggerFactory.getLogger(this.getClass());

	public void validateCpService(Long cpid, Response expected) {

		SoftAssert softAssert = new SoftAssert();
		
		try {
			
			String json = expected.asString();
			
			if(pathExists(json,"$.rmm_generic")) {
				RmmGenericEntity cpEntity = cpHelper.getRmmGenericData(cpid);
				
				List<DbMap> expectedList = expected.body().jsonPath().getList("rmm_generic", DbMap.class);
				
				expectedList.forEach(it -> {

					String key = it.getKey();
					Object expectedValue = it.getValue();
					String type = it.getType();
					
					try {
						Object actual = (Object) cpEntity.getClass().getField(key).get(cpEntity);

						validateAssertion(softAssert, type, expectedValue, actual);

					} catch (IllegalArgumentException | IllegalAccessException | NoSuchFieldException
							| SecurityException e) {

						logger.error(CPValidator.class.getName(), e.getMessage());
					}
				});
			} 
			
			if(pathExists(json,"$.content_partner")) {

				ContentPartnerEntity cpEntity = cpHelper.getCpData(cpid);
				
				List<DbMap> expectedList = expected.body().jsonPath().getList("content_partner", DbMap.class);
				
				expectedList.forEach(it -> {
	
					String key = it.getKey();
					Object expectedValue = it.getValue();
					String type = it.getType();
					
					try {
						Object actual = (Object) cpEntity.getClass().getField(key).get(cpEntity);
	
						validateAssertion(softAssert, type, expectedValue, actual);
	
					} catch (IllegalArgumentException | IllegalAccessException | NoSuchFieldException
							| SecurityException e) {
	
						logger.error(CPValidator.class.getName(), e.getMessage());
					}
				});
			}
			
			softAssert.assertAll();
			
		} catch (Exception e) {
			softAssert.assertTrue(false);
			logger.error("Error", e);
		}

		softAssert.assertAll();
	}

}
