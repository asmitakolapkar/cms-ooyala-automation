package com.vuclip.cms2.validator;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.configurationprocessor.json.JSONException;
import org.springframework.stereotype.Component;
import org.testng.asserts.SoftAssert;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.vuclip.cms2.entity.CountryEntity;
import com.vuclip.cms2.entity.LanguageEntity;
import com.vuclip.cms2.model.DbMap;

import io.restassured.response.Response;

@Component
public class RefDataValidator extends BaseValidator {
	
	private final Logger logger = LoggerFactory.getLogger(this.getClass());
	
	JsonParser parser = new JsonParser();

	public void validateCountryService(CountryEntity country, Response response) {

		SoftAssert softAssert = new SoftAssert();
		try {
			List<DbMap> expectedList = response.body().jsonPath().getList("country", DbMap.class);

			expectedList.forEach(it -> {

				String key = it.getKey();
				Object expectedValue = it.getValue();
				String type = it.getType();

				try {
					Object actual = (Object) country.getClass().getField(key).get(country);

					validateAssertion(softAssert, type, expectedValue, actual);

				} catch (IllegalArgumentException | IllegalAccessException | NoSuchFieldException
						| SecurityException e) {

					logger.error(RefDataValidator.class.getName(), e.getMessage());
				}

			});

			softAssert.assertAll();

		} catch (Exception e) {
			softAssert.assertTrue(false);
			logger.error("Error", e);
		}

		softAssert.assertAll();
	}
	
	public void validateLanguageService(LanguageEntity language, Response response) {

		SoftAssert softAssert = new SoftAssert();
		try {
			List<DbMap> expectedList = response.body().jsonPath().getList("ct_language", DbMap.class);

			expectedList.forEach(it -> {

				String key = it.getKey();
				Object expectedValue = it.getValue();
				String type = it.getType();

				try {
					Object actual = (Object) language.getClass().getField(key).get(language);

					validateAssertion(softAssert, type, expectedValue, actual);

				} catch (IllegalArgumentException | IllegalAccessException | NoSuchFieldException
						| SecurityException e) {

					logger.error(RefDataValidator.class.getName(), e.getMessage());
				}

			});

			softAssert.assertAll();

		} catch (Exception e) {
			softAssert.assertTrue(false);
			logger.error("Error", e);
		}

		softAssert.assertAll();
	}

	public List<String> validateResponse(Response expectedResponse, Response actualResponse) throws JSONException {

		JsonObject expectedJsonObject = (JsonObject) parser.parse(expectedResponse.getBody().asString());

		JsonObject actualJsonObject = (JsonObject) parser.parse(actualResponse.getBody().asString());

		return assertJsonData(expectedJsonObject.getAsJsonObject("json"), actualJsonObject);

	}
	
	public void validateResponseList(Response rsp, String field) {
		SoftAssert softAssert = new SoftAssert();
		
		softAssert.assertTrue(rsp.body().jsonPath().getList(field).size()>0, 
				"Expected : Greater than 0,  Actual :"+rsp.body().jsonPath().getList(field).size());

		softAssert.assertAll();
	}
}
