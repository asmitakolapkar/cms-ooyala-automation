package com.vuclip.cms2.validator;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathFactory;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.configurationprocessor.json.JSONException;
import org.springframework.stereotype.Component;
import org.testng.asserts.SoftAssert;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.jcabi.xml.XMLDocument;
import com.vuclip.cms2.model.DbMap;
import com.vuclip.cms2.util.XmlNodeNotFoundException;

import static org.junit.Assert.assertThat;
import static org.valid4j.matchers.jsonpath.JsonPathMatchers.*;

@Component
public class BaseValidator {

	XPathFactory xPathfactory = XPathFactory.newInstance();

	XPath xPath = xPathfactory.newXPath();

	private final Logger logger = LoggerFactory.getLogger(this.getClass());

	public List<String> failedKeys = new ArrayList<>();

	private List<String> failedXmlKeys;

	public SoftAssert validateAssertion(SoftAssert softAssert, String type, Object expected, Object actual) {

		try {
			if (type == null || type.equals("String")) {
				logger.info("asserting string  value=" + expected.toString());
				softAssert.assertEquals(actual.toString(), (String) expected, "Expected : "+expected+" Actual :"+actual);

			} else if (type.equals("Long")) {

				logger.info("asserting  value of Long=" + Long.parseLong(expected.toString()));
				softAssert.assertEquals((Long) actual, Long.parseLong(expected.toString()), "Expected : "+expected+" Actual :"+actual);
			} else if (type.equals("Integer")) {

				logger.info("asserting db value of Integer=" + expected.toString());
				softAssert.assertEquals((Integer) actual, (Integer) expected, "Expected : "+expected+" Actual :"+actual);
			} else if(type.equals("BLOB")) {
				logger.info("asserting  value of Blob=" + expected.toString());
				//softAssert.assertEquals(actual.toString().substring(1), (String) expected, "Expected : "+expected+" Actual :"+actual);
			}

		} catch (IllegalArgumentException | SecurityException e) {

			e.printStackTrace();
			logger.error(BaseValidator.class.getName(), e.getMessage());
		}

		return softAssert;

	}

	public static boolean pathExists(String json, String jsonPath) {
	    try {
	        assertThat(json, hasJsonPath(jsonPath));
	        return true;
	    } catch (AssertionError ae) {
	    	return false;
	    } catch (Exception e) {
	        return false;
	    }
	}   
	
	public List<String> assertJsonData(JsonObject js1, JsonObject js2) {

		Set<String> l1 = js1.keySet();

		for (String key : l1) {

			try {
				Object val1 = js1.get(key);
				Object val2 = js2.get(key);

				if (val1 instanceof JsonObject && val2 != null) {

					assertJsonData((JsonObject) val1, (JsonObject) val2);

				} else if (val1 instanceof JsonArray && val2 != null) {

					JsonArray list1 = (JsonArray) val1;

					JsonArray list2 = (JsonArray) val2;

					if (list1.size() == list2.size()) {

						for (int i = 0; i < list1.size(); i++) {
							if(list1.get(i) instanceof JsonObject)
								assertJsonData(list1.get(i).getAsJsonObject(), list2.get(i).getAsJsonObject());
							else if(list1.get(i).equals(list2.get(i)))
								System.out.println(" validated for key=" + list1.get(i));
						}
					} else {
						System.out.println("json array size not equal for =" + key);

					}

				}

				else if (val2 != null && val1.equals(val2)) {

					// System.out.println(" validated for key=" + key);

				} else {
					System.out.println("not validated for key=" + key + "Actual Value : "+val2+"Expeccted value : "+val1);

					failedKeys.add(key);
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}

		return failedKeys;

	}

	public List<String> assertXmlData(List<DbMap> expectedDataArray, Document doc) throws JSONException {

		failedXmlKeys = new ArrayList<>();
		
		expectedDataArray.forEach(it -> {

			String key = it.getKey();
			Object expectedValue = it.getValue();

			try {
				Object nodeValue = getXMLNodeValue(key, doc);

				if (!expectedValue.equals(nodeValue)) {
					failedXmlKeys.add(key);
				} else {
					logger.info("validated for key = " + key);
				}

			} catch (XmlNodeNotFoundException e) {

				failedXmlKeys.add(key);
				e.printStackTrace();
			}

		});

		return failedXmlKeys;

	}

	public String getXMLNodeValue(String xPathValue, Document doc) throws XmlNodeNotFoundException {
		String value = "";
		String xml = "";
		try {

			xml = new XMLDocument(doc).toString();

			NodeList nodes = null;

			nodes = (NodeList) xPath.compile(xPathValue).evaluate(doc, XPathConstants.NODESET);

			if (nodes == null || nodes.getLength() == 0) {
				logger.error("No Value found for xpath==" + xPathValue);

			}

			value = nodes.item(0).getTextContent();
		} catch (Exception e) {
			logger.error("No Value found for xpath==" + xPathValue + " xml==" + xml);
			throw new XmlNodeNotFoundException("No Node/value found in clip xml for node==" + xPathValue + "\n");
		}

		return value;

	}

}
