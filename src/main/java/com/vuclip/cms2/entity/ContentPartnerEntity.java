package com.vuclip.cms2.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

import lombok.Data;

@Data
@Entity(name = "content_partner")
public class ContentPartnerEntity implements Serializable{

	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "content_partner_id")
    public Long content_partner_id;

    @Column(name = "content_partner_name")
    public String content_partner_name;
	
}
