package com.vuclip.cms2.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

import lombok.Getter;
import lombok.Setter;

@Entity(name = "country")
@Getter
@Setter
public class CountryEntity implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "countryId")
	public Long Id;

	@Column(name = "continent")
	public String continent;

	@Column(name = "countryA2")
	public String a2Code;

	@Column(name = "countryA3")
	public String a3Code;

	@Column(name = "countryN3")
	public String countryN3;

	@Column(name = "countryName")
	public String longName;

	@Column(name = "active")
	public String active;

	@Column(name = "updatetime")
	public Date updatetime;

}
